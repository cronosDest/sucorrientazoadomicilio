package Test;

import static org.junit.Assert.*;

import com.sucorrientazoadomicilio.commons.exception.ConflictException;
import com.sucorrientazoadomicilio.domain.DroneDTO;
import com.sucorrientazoadomicilio.domain.LocationDroneReportDTO;
import com.sucorrientazoadomicilio.domain.ReportDroneDTO;
import com.sucorrientazoadomicilio.service.impl.DeliveryImpl;
import com.sucorrientazoadomicilio.utils.Constant;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DeliveryTest {

    DeliveryImpl deliveryImpl;
    List<LocationDroneReportDTO> locationDroneList = new ArrayList<LocationDroneReportDTO>();
    ReportDroneDTO reportDroneDTO = new ReportDroneDTO();
    DroneDTO droneDTO;

    @Before
    public void configureTest() {
        deliveryImpl = new DeliveryImpl();

        locationDroneList.add(new LocationDroneReportDTO(-2, 4, 1));
        locationDroneList.add(new LocationDroneReportDTO(-1, 3, 1));
        locationDroneList.add(new LocationDroneReportDTO(0, 0, 1));
        droneDTO = new DroneDTO("01");
        reportDroneDTO.setDroneDTO(droneDTO);
        reportDroneDTO.setLocationDroneList(locationDroneList);
    }

    @Test
    public void writeReportTest() {
        boolean result = deliveryImpl.writeReport(reportDroneDTO);
        assertTrue(result);
    }

    @Test
    public void validateDeliveryScopeTest() throws ConflictException {
        boolean result = deliveryImpl.validateDeliveryScope(droneDTO);
        assertTrue(result);
    }

    @Test
    public void moveAheadDroneTest() throws ConflictException {
        DroneDTO newDrone = new DroneDTO("01");
        deliveryImpl.moveAhead(newDrone);
        assertNotEquals(droneDTO, newDrone);
    }

    @Test
    public void moveRightTest() {
        deliveryImpl = new DeliveryImpl();
        deliveryImpl.moveRight();
        assertEquals(Constant.ORIENTE, deliveryImpl.getOrientation());
    }

    @Test
    public void moveLeftTest() {
        deliveryImpl = new DeliveryImpl();
        deliveryImpl.moveLeft();
        assertEquals(Constant.OCCIDENTE, deliveryImpl.getOrientation());
    }

}
