package com.sucorrientazoadomicilio.service.impl;

import com.sucorrientazoadomicilio.commons.Validator;
import com.sucorrientazoadomicilio.commons.exception.ConflictException;
import com.sucorrientazoadomicilio.domain.DroneDTO;
import com.sucorrientazoadomicilio.domain.DroneDeliveriesDTO;
import com.sucorrientazoadomicilio.domain.LocationDroneReportDTO;
import com.sucorrientazoadomicilio.domain.ReportDroneDTO;
import com.sucorrientazoadomicilio.service.IDelivery;
import com.sucorrientazoadomicilio.utils.ApplicationProperties;
import com.sucorrientazoadomicilio.utils.Constant;
import com.sucorrientazoadomicilio.utils.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Orlando Velasquez
 */
public class DeliveryImpl implements IDelivery {
    private static final Logger logger = LogManager.getLogger(DeliveryImpl.class);

    FileUtils fileUtils = new FileUtils();
    private int orientation = Constant.NORTE;
    private Validator validator = new Validator();

    /**
     * Used to send deliveries
     */
    public void sendDeliveries() {
        logger.info("DeliveryImpl :: sendDeliveries :: sending deliveries");
        List<DroneDeliveriesDTO> droneDeliveriesDTOList = loadDeliveries();
        for (DroneDeliveriesDTO droneDeliveriesDTO : droneDeliveriesDTOList) {
            orientation = Constant.NORTE;
            sendDrone(droneDeliveriesDTO);
        }
    }

    /**
     * Used to read the deliveries from txt files and save in a list
     *
     * @return list to delivered by each drone
     */
    public List<DroneDeliveriesDTO> loadDeliveries() {
        logger.info("DeliveryImpl :: loadDeliveries :: loading deliveries");
        List<DroneDeliveriesDTO> droneDeliveriesDTOList = new ArrayList<DroneDeliveriesDTO>();

        int droneNumber = Integer.parseInt(ApplicationProperties.getInstance().getProperty("droneNumber"));
        for (int i = 1; i <= droneNumber; i++) {
            DroneDeliveriesDTO droneDeliveriesDTO = new DroneDeliveriesDTO();
            String droneId = Integer.toString(i).length() == 1 ? "0".concat(Integer.toString(i)) : Integer.toString(i);

            List<String> deliveries = fileUtils.readDeliveries("in".concat(droneId).concat(".txt"));
            if (validator.checkOwnerInstructions(deliveries, droneId)) {
                droneDeliveriesDTO.setDeliveries(deliveries);
                droneDeliveriesDTO.setDroneDTO(new DroneDTO(droneId));
                droneDeliveriesDTOList.add(droneDeliveriesDTO);
            }
        }
        return droneDeliveriesDTOList;
    }

    /**
     * Used to move each drone on the grid and write the report
     *
     * @param droneDeliveriesDTO deliveries by each drone
     */
    public void sendDrone(DroneDeliveriesDTO droneDeliveriesDTO) {
        logger.info("DeliveryImpl :: sendDrone :: sending drone");
        ReportDroneDTO reportDroneDTO = new ReportDroneDTO();
        reportDroneDTO.setDroneDTO(droneDeliveriesDTO.getDroneDTO());
        try {
            for (String delivery : droneDeliveriesDTO.getDeliveries()) {
                moveDrone(delivery, droneDeliveriesDTO.getDroneDTO());
                reportDroneDTO.getLocationDroneList().add(
                        new LocationDroneReportDTO(droneDeliveriesDTO.getDroneDTO().getPosX(),
                                droneDeliveriesDTO.getDroneDTO().getPosY(), orientation));
            }
        } catch (ConflictException ex) {
            LocationDroneReportDTO locationDroneReportDTO = new LocationDroneReportDTO();
            locationDroneReportDTO.setValid(false);
            reportDroneDTO.getLocationDroneList().add(locationDroneReportDTO);
        }
        writeReport(reportDroneDTO);
    }

    /**
     * Used to move the drone in the allowed positions (A,I,D)
     *
     * @param delivery The instructions to move the drone
     * @param droneDTO drone to move
     * @throws ConflictException exception when the instructions violate the instructions allowed
     */
    public void moveDrone(String delivery, DroneDTO droneDTO) throws ConflictException {
        logger.info("DeliveryImpl :: moveDrone :: moving drone");
        String[] instructions = delivery.split("");
        for (String instruction : instructions) {
            if (instruction.equalsIgnoreCase("A")) {
                moveAhead(droneDTO);
            } else if (instruction.equalsIgnoreCase("I")) {
                moveLeft();
            } else if (instruction.equalsIgnoreCase("D")) {
                moveRight();
            }
        }
    }

    /**
     * Used to move the drone position in the grid
     *
     * @param droneDTO drone to move
     * @throws ConflictException exception when the instructions violate the instructions allowed
     */
    public void moveAhead(DroneDTO droneDTO) throws ConflictException {
        logger.info("DeliveryImpl :: moveAhead :: moving ahead");
        byte dronePosX = droneDTO.getPosX();
        byte dronePosY = droneDTO.getPosY();
        switch (orientation) {
            case Constant.NORTE:
                dronePosY++;
                droneDTO.setPosY(dronePosY);
                break;
            case Constant.ORIENTE:
                dronePosX++;
                droneDTO.setPosX(dronePosX);
                break;
            case Constant.SUR:
                dronePosY--;
                droneDTO.setPosY(dronePosY);
                break;
            case Constant.OCCIDENTE:
                dronePosX--;
                droneDTO.setPosX(dronePosX);
                break;
        }
        validateDeliveryScope(droneDTO);
    }

    /**
     * Used to change the drone orientation to the right
     */
    public void moveRight() {
        logger.info("DeliveryImpl :: moveRight :: moving right");
        changeOrientation(Constant.RIGHT);
    }

    /**
     * Used to change the drone orientation to the left
     */
    public void moveLeft() {
        logger.info("DeliveryImpl :: moveLeft :: moving left");
        changeOrientation(Constant.LEFT);
    }

    /**
     * Used to change the drone orientation
     */
    public void changeOrientation(int newMovement) {
        logger.info("DeliveryImpl :: changeOrientation :: changing orientation");
        switch (newMovement) {
            case Constant.RIGHT:
                if (orientation == 4) {
                    orientation = Constant.NORTE;
                    break;
                }
                orientation++;
                break;
            case Constant.LEFT:
                if (orientation == 1) {
                    orientation = Constant.OCCIDENTE;
                    break;
                }
                orientation--;
                break;
        }
    }

    /**
     * Used to validate if the drone position is allowed
     *
     * @param droneDTO drone to validate
     * @return boolean to determinate if is correct
     * @throws ConflictException exception when the instructions violate the instructions allowed
     */
    public boolean validateDeliveryScope(DroneDTO droneDTO) throws ConflictException {
        logger.info("DeliveryImpl :: validateDeliveryScope :: validating deliveries");
        int blockNumber = Integer.parseInt(ApplicationProperties.getInstance().getProperty("blockNumber"));
        if (droneDTO.getPosX() > blockNumber || droneDTO.getPosX() < -blockNumber
                || droneDTO.getPosY() > blockNumber || droneDTO.getPosY() < -blockNumber) {
            throw new ConflictException("la entrega esta mas alla del alcance del dron.");
        }
        return true;
    }

    /**
     * Use to write the report in a txt document
     *
     * @param reportDroneDTO
     * @return
     */
    public boolean writeReport(ReportDroneDTO reportDroneDTO) {
        logger.info("DeliveryImpl :: writeReport :: writing report");
        return fileUtils.writeFileReport("out".concat(reportDroneDTO.getDroneDTO().getDroneId()).concat(".txt"),
                reportDroneDTO.getLocationDroneList());
    }

    public int getOrientation() {
        return orientation;
    }

}
