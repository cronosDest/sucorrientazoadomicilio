package com.sucorrientazoadomicilio.service;

import com.sucorrientazoadomicilio.domain.DroneDeliveriesDTO;

import java.util.List;

/**
 * @author Orlando Velasquez
 */
public interface IDelivery {

    public void sendDeliveries();

    public List<DroneDeliveriesDTO> loadDeliveries();

    public void sendDrone(DroneDeliveriesDTO droneDeliveriesDTO);
}
