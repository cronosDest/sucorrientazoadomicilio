/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sucorrientazoadomicilio;

import com.sucorrientazoadomicilio.service.impl.DeliveryImpl;

/**
 * @author Orlando Velasquez
 */
public class Main {

    /**
     * @param args initial arguments
     */
    public static void main(String[] args) {
        DeliveryImpl deliveryImpl = new DeliveryImpl();
        deliveryImpl.sendDeliveries();
    }

}
