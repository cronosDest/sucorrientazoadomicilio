package com.sucorrientazoadomicilio.commons;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Orlando Velasquez
 */
public class Validator {

    private static final Logger logger = LogManager.getLogger(Validator.class);

    public boolean checkOwnerInstructions(List<String> deliveries, String droneId) {
        logger.info("Validator :: checkOwnerInstructions :: checking owner instructions");
        for (String delivery : deliveries) {
            if (!delivery.contains("A") && !delivery.contains("D") && !delivery.contains("I")) {
                logger.warn("invalid Instructions to drone: ".concat(droneId));
                return false;
            }
        }
        return true;
    }

}
