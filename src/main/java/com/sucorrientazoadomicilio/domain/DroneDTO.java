package com.sucorrientazoadomicilio.domain;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class DroneDTO {

    public DroneDTO(String droneId) {
        this.droneId = droneId;
        posX = 0;
        posY = 0;
    }

    private String droneId;
    private byte posX;
    private byte posY;
}
