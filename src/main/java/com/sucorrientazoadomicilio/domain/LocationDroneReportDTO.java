package com.sucorrientazoadomicilio.domain;

import com.sucorrientazoadomicilio.utils.Constant;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LocationDroneReportDTO {

    public LocationDroneReportDTO(int posX, int posY, int orientation) {
        this.posX = posX;
        this.posY = posY;

        switch (orientation) {
            case Constant.NORTE:
                this.orientation = "direccion Norte";
                break;
            case Constant.ORIENTE:
                this.orientation = "direccion Oriente";
                break;
            case Constant.SUR:
                this.orientation = "direccion Sur";
                break;
            case Constant.OCCIDENTE:
                this.orientation = "direccion Occidente";
                break;
        }
    }

    private int posX;
    private int posY;
    private String orientation;
    private boolean isValid = true;

}
