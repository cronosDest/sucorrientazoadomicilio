package com.sucorrientazoadomicilio.domain;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class DroneDeliveriesDTO {

    List<String> deliveries;
    DroneDTO droneDTO;

}
