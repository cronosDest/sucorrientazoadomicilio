package com.sucorrientazoadomicilio.domain;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ReportDroneDTO {

    private DroneDTO droneDTO;
    private List<LocationDroneReportDTO> locationDroneList = new ArrayList<LocationDroneReportDTO>();

}
