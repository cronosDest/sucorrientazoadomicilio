package com.sucorrientazoadomicilio.utils;

import com.sucorrientazoadomicilio.domain.LocationDroneReportDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Orlando Velasquez
 */
public class FileUtils {
    private static final Logger logger = LogManager.getLogger(FileUtils.class);

    /**
     * Used to read the txt file
     * @param fileName txt fileName
     * @return List of instructions to drone
     */
    public List<String> readDeliveries(String fileName) {
        logger.info("FileUtils :: readDeliveries :: reading deliveries");
        List<String> deliveries = new ArrayList<String>();
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("resources/input/" + fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                deliveries.add(line);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    /**
     * Used to write in the txt file
     * @param fileName txt fileName
     * @param locationDroneReportDTOList report to write in the txt file
     * @return boolean to validate if the write is correct
     */
    public boolean writeFileReport(String fileName, List<LocationDroneReportDTO> locationDroneReportDTOList) {
        logger.info("FileUtils :: writeFileReport :: writing file report");
        try {
            FileWriter myWriter = new FileWriter("resources/output/" + fileName);
            myWriter.write("== Reporte de entregas ==\n\n");

            for (LocationDroneReportDTO locationDroneReportDTO : locationDroneReportDTOList) {
                StringBuilder sb = new StringBuilder();
                if (!locationDroneReportDTO.isValid()) {
                    int blockNumber = Integer.parseInt(ApplicationProperties.getInstance().getProperty("blockNumber"));
                    sb.append("El alcance solicitado excede el permitido el cual es ").append(blockNumber).append(" Cuadras.");
                    myWriter.write(sb.toString());
                    continue;
                }
                sb.append("(").append(locationDroneReportDTO.getPosX()).append(",");
                sb.append(locationDroneReportDTO.getPosY()).append(") ");
                sb.append(locationDroneReportDTO.getOrientation());
                sb.append("\n");
                myWriter.write(sb.toString());
            }
            myWriter.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
