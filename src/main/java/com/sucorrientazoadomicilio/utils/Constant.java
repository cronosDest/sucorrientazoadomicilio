package com.sucorrientazoadomicilio.utils;

/**
 * @author Orlando Velasquez
 */
public class Constant {

    public static final int NORTE = 1;
    public static final int ORIENTE = 2;
    public static final int SUR = 3;
    public static final int OCCIDENTE = 4;

    public static final int RIGHT = 1;
    public static final int LEFT = 2;
}
